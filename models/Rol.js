// Importaciones
import Sequelize from "sequelize";
import db from "../config/db.js";

// Creacion del modelo
const Rol = db.define('roles', {
    nombre:{
        type: Sequelize.STRING
    }
});

// Exportacion del modelo
export default Rol;