// Importaciones
import Sequelize from "sequelize";
import db from "../config/db.js";

// Modelo
const Usuario = db.define('usuarios', {
    nombre:{
        type: Sequelize.STRING
    },
    apellido:{
        type: Sequelize.STRING
    },
    correo:{
        type: Sequelize.STRING
    },
    clave:{
        type: Sequelize.STRING
    },
    rol_id:{
        type: Sequelize.INTEGER
    },
    seleccion_id:{
        type: Sequelize.INTEGER
    },
    created_at:{
        type: Sequelize.DATE
    },
    updated_at:{
        type: Sequelize.DATE
    }
});

export default Usuario;