// Importaciones
import Sequelize from "sequelize";
import db from "../config/db.js";

// Modelo
const Seleccion = db.define('selecciones', {
    encuesta_id: {
        type: Sequelize.INTEGER
    },
    usuario_id: {
        type: Sequelize.INTEGER
    },
    curso_1_id: {
        type: Sequelize.INTEGER
    },
    curso_2_id: {
        type: Sequelize.INTEGER
    },
})

// Exportacion
export default Seleccion;