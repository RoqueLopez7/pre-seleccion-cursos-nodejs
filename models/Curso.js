// Importaciones
import Sequelize from "sequelize";
import db from "../config/db.js";

// Modelo
const Curso = db.define('cursos', {
    nombre:{
        type: Sequelize.STRING
    },
    created_at:{
        type: Sequelize.DATE
    },
    updated_at:{
        type: Sequelize.DATE
    }
});

export default Curso;