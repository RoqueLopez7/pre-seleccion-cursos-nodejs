// Importaciones
import Sequelize from "sequelize";
import db from "../config/db.js";

// Modelo
const Encuesta = db.define('encuestas', {
    fecha_inicio: {
        type: Sequelize.DATE
    },
    fecha_fin: {
        type: Sequelize.DATE
    },
    status:{
        type: Sequelize.BOOLEAN
    },
    // usuario_id:{
    //     type: Sequelize.INTEGER
    // },
    created_at:{
        type: Sequelize.DATE
    },
    updated_at:{
        type: Sequelize.DATE
    }
});

// Exportamos el modelo
export default Encuesta;