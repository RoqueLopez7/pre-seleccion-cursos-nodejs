// Importaciones
import Sequelize  from "sequelize";

// Conexion con la base de datos y configuracion de la libreria
const db = new Sequelize('pre_seleccion_cursos', 'roque', '123456',{
    host:'127.0.0.1',
    port:'3306',
    dialect: 'mysql',
    define: {
        timestamps: false,
    },
    pool:{
        max:5,
        min:0,
        acquire: 30000,
        idle: 10000
    },
    operatorAliases: false
});

// Exportamos la constante
export default db