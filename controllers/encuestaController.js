// Importaciones
import Encuesta from "../models/Encuesta.js";

const obtenerEncuestas = async(req, res) =>{
    try {
        // Buscar todas las encuestas
        const encuestas = await Encuesta.findAll();

        console.log(encuestas);

        // Renderizar las encuestas 
        return res.render('encuestas', { encuestas })
    } catch (error) {
        console.log(error);
    }
}

const agregarEncuesta = async(req, res) =>{
    // Variables
    let { fecha_inicio, fecha_fin } = req.body;

    try {
        // Formateo de la fecha
        fecha_inicio = new Date(fecha_inicio);
        fecha_fin = new Date(fecha_fin);

        // Busqueda de todas las encuestas
        const encuestas = await Encuesta.findAll();
        
        // Validacion
        if (fecha_inicio > fecha_fin) {
            return res.render('encuestas',{
                mensaje: "La fecha de inicio no puede ser mayor a la fecha de finalizacion",
                encuestas
            });
        }

        // Busca todas las encuestas que ya esten registradas y esten en ese periodo
        const encuestasDB = await Encuesta.findAll({ where: { fecha_fin: { lt: fecha_fin }, status: true }});
        
        // Validacion
        if (encuestasDB.length > 0) {
            return res.render('encuestas',{
                mensaje: "Ya hay una encuesta activa para ese periodo de tiempo.",
                encuestas
            });
        }

        // Se guarda la encuesta
        await Encuesta.create({
            fecha_inicio,
            fecha_fin,
            status: true
        });

        // Renderizar la vista de las encuestas
        return res.redirect('/encuestas');
    } catch (error) {
        console.log(error);
    }
}

const eliminarEncuesta = async(req, res) =>{
    // Variables
    const { id } = req.params;

    try {
        // Buscando las encuestas
        const encuesta = await Encuesta.findOne({ where: { id }});

        // Si hay encuesta
        if (encuesta) {
            // Se borra la encuesta 
            encuesta.destroy();

            // Regresa a la vista de encuestas
            res.redirect('/encuestas');
        }else{

            // Buscar todas las encuestas
            const encuestas = await Encuesta.findAll();

            // se renderiza el template pero con el mensaje de que no existe el curso
            return res.render('cursos', {
                mensaje: 'Hubo un error al eliminar la encuesta, al parecer no existe en la base de datos.',
                encuestas
            })
        }
    } catch (error) {
        console.log(error);
    }
}


// Editar encuesta
const editarEncuesta = async(req, res) =>{
    // variables
    const { id } = req.params;
    let { fecha_inicio, fecha_fin } = req.body;

    // Validacion
    if ([fecha_inicio, fecha_fin].includes('')) {
        return res.render('editar-encuesta', {
            mensaje: 'Todos los campos son es obligatorio',
            encuesta: { id, fecha_inicio, fecha_fin },
        })
    }

    try {

        // Formateo de la fecha
        fecha_inicio = new Date(fecha_inicio);
        fecha_fin = new Date(fecha_fin);

        // Busqueda de todas las encuestas
        const encuestas = await Encuesta.findAll();
        
        // Validacion
        if (fecha_inicio > fecha_fin) {
            return res.render('editar-encuesta',{
                mensaje: "La fecha de inicio no puede ser mayor a la fecha de finalizacion",
                encuestas
            });
        }

        // Busca todas las encuestas que ya esten registradas y esten en ese periodo
        const encuestasDB = await Encuesta.findAll({ where: { fecha_fin: { lt: fecha_fin }, status: true }});
        
        // Validacion
        if (encuestasDB.length > 0) {
            return res.render('encuestas',{
                mensaje: "Ya hay una encuesta activa para ese periodo de tiempo.",
                encuesta: { id, fecha_inicio, fecha_fin },
            });
        }

        let encuesta = await Encuesta.findOne({ where: { id }});

        if (encuesta) {
            // Se guardan los cambios de la encuesta
            await encuesta.update({
                fecha_inicio,
                fecha_fin,
                status: true
            });
        }else{
            return res.render('editar-encuesta', {
                error: true,
                encuesta: { id, fecha_inicio, fecha_fin },
            })
        }


        // Renderizar la vista de las encuestas
        return res.redirect('/encuestas');
    } catch (error) {
        console.log(error);
    }
}

const verEncuesta = async(req, res) =>{

    const {id} = req.params;
    console.log(id);

    try {
        let {dataValues} = await Encuesta.findOne({ where: { id }});
        let encuesta = dataValues
        // console.log(encuesta);

        if (encuesta) {
            res.render('editar-encuesta', {encuesta})
        }else{
            res.render('editar-encuesta', {
                error:true
            })
        }

    } catch (error) {
        console.log(error);
    }
}

// Exportar funciones
export {
    obtenerEncuestas,
    agregarEncuesta,
    eliminarEncuesta,
    editarEncuesta,
    verEncuesta
}