// Encuesta
import Usuario from '../models/Usuario.js';
import Encuesta from '../models/Encuesta.js';
import Curso from '../models/Curso.js';
import Rol from '../models/Rol.js';

// Controladores
const paginaInicio = (req, res) => {
    res.render('login');
}

const autenticarUsuario = async(req, res) => {
    // Extraigo el correo y la clave
    const {correo, clave} = req.body;
    
    // Objeto a retornar
    let mensaje = '';
    let vista = 'login';
    let usuario;

    // Validacion si al menos uno esta vacio
    if ([correo, clave].includes('')) {

        // Preparacion del mensaje
        mensaje = 'Todos los campos son obligatorios';
    }else{
        // Busqueda del usuario con ese correo
        usuario = await Usuario.findAll({ where: { correo }});

        // Si no encuentra a ningun usuario
        if (usuario.length === 0) {
            // Preparacion del mensaje
            mensaje = 'No hay existe ningun usuario asociado con ese correo, puede registrarse en el sistema.'
        }else{
            // Se le asigna el valor de los datos necesarios a la variable
            usuario = usuario[0].dataValues;

            // Si la clave es igual a la enviada
            if (usuario.clave === clave) {

                // Prepracion del mensaje
                mensaje = `Bienvenid@ ${usuario.nombre}`;

                // Cambio de Vista
                vista = 'home';

                // Se autentica al usuario
                req.session.usuario = usuario.nombre;

                // Se busca el rol del usuario
                let rol = await Rol.findOne({ where: { id: usuario.rol_id }});

                // Asignando el nombre del rol
                req.session.rol = rol['dataValues']['nombre'];
            }else{
                // Preparacion del mensaje
                mensaje = 'Clave no coincide con la registrada, intente de nuevo.';
            }
        }
    }

    // Si la vista sigue siendo login
    if(vista === 'login'){
        // Renderizar la vista y su mensaje
        res.render(vista, { mensaje });
    }else{
        // Redireccion por haberse logeado
        res.redirect('/home');
        // paginaHome({usuario_id});
    }
}

const paginaHome = async(req, res) => {

    // Buscar la encuesta que este activa
    let encuesta = await Encuesta.findOne({ where: { status: true }});

    // Si hay una encuesta activa
    if (encuesta) {
        // Busqueda de todos los cursos
        const cursosDB = await Curso.findAll();
        let cursos = [];
    
        for (let i = 0; i < cursosDB.length; i++) {
            cursos.push(cursosDB[i]['dataValues']);
        }

        console.log('Vista Home');
        console.log(req.session);
    
        res.render('home', { cursos, encuestaStatus: true, rol: req.session.rol });
    }else{
        res.render('home', { 
            encuestaStatus: false,
            cursos: []
        });
    }
}

const paginaCursos = async(req, res) =>{
    // Busqueda de todos los cursos
    const cursosDB = await Curso.findAll();
    let cursos = [];

    for (let i = 0; i < cursosDB.length; i++) {
        cursos.push(cursosDB[i]['dataValues']);
    }
    
    // Renderizado de la vista con los cursos
    res.render('cursos', { cursos });
}

// Funcion para cerrar sesion
const cerrarSesion = (req, res) =>{
    // Si hay una sesion
    if (req.session) {
        // La elimina
        req.session.destroy();
    }

    // Redireccion al login
    res.redirect('/');
}

// Exportar funciones
export{
    paginaInicio,
    autenticarUsuario,
    paginaHome,
    paginaCursos,
    cerrarSesion
}