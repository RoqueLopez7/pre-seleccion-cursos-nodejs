// Importaciones
import Curso from "../models/Curso.js";

// Funciones
const agregarCurso = async(req,res) => {

    // Destructuring del objeto body que trae la informacion del formulario
    let { nombre } = req.body;

    // Validacion
    if (nombre.trim() === '') {
        return res.render('cursos', {
            mensaje: 'Debe agregar un nombre para poder guardar un curso'
        });
    }

    // Reasignacion para que venga en minuscula
    nombre = nombre.toLowerCase();

    try {
        // Busca todos los cursos con ese nombre
        let cursosDB = await Curso.findAll({ where: { nombre }});

        // Validacion por si ya existe el curso
        if (cursosDB.length > 0) {
            cursosDB = await Curso.findAll()

            let cursos = [];

            for (let i = 0; i < cursosDB.length; i++) {
                cursos.push(cursosDB[i]['dataValues']);
            }
            
            return res.render('cursos', {
                mensaje: 'Este curso ya existe en la base de datos',
                cursos
            });
        }

        // Se crea el registro
        Curso.create({ nombre });

        return res.redirect('/cursos')
    } catch (error) {
        console.log(error);
    }
}

// Ver curso
const verCurso = async(req, res)=>{

    // Extraemos el id desde los parametros de la ruta
    const { id } = req.params;

    try {
        let curso = await Curso.findOne({ where: { id } });

        console.log(curso);
        // curso = curso.dataValues;
        if (curso) {
            return res.render('editar-curso', {
                curso: curso.dataValues,
                error: false
            })
        }else{
            return res.render('editar-curso', {     
                error: true 
            })
        }
    } catch (error) {
        console.log(error);
    }
}

// Editar curso
const editarCurso = async(req, res) =>{
    // variables
    const { id } = req.params;
    let { nombre } = req.body;

    // Validacion
    if (nombre.trim() === "") {
        return res.render('editar-curso', {
            mensaje: 'El nombre es obligatorio',
            curso: { id, nombre },
        })
    }

    try {

        // Busqueda de algun curso con el mismo nombre
        let curso = await Curso.findAll({ where: { nombre }});

        // Validacion si encuentra un curso
        if (curso) {
            return res.render('editar-curso', {
                mensaje: 'Esta asignatura ya esta registrada',
                curso: { id, nombre },
            })
        }

        // Buscando el curso
        curso = await Curso.findOne({ where: { id }});

        // Si hay curso
        if (curso) {
            // Se edita el curso 
            curso.update({ nombre: nombre.toLowerCase() });

            // Regresa a la vista de cursos
            res.redirect('/cursos');
        }else{
            // se renderiza el template pero con el mensaje de que no existe el curso
            return res.render('editar-curso', {
                error:true,
                curso: { id, nombre },
            })
        }
    } catch (error) {
        console.log(error);
    }
}

const eliminarCurso = async(req, res) =>{
    // Variables
    const { id } = req.params;

    try {
        // Buscando el curso
        const curso = await Curso.findOne({ where: { id }});

        // Si hay curso
        if (curso) {
            // Se edita el curso 
            curso.destroy();

            // Regresa a la vista de cursos
            res.redirect('/cursos');
        }else{

            // Buscar todos los cursos
            const cursos = await Curso.findAll();

            // se renderiza el template pero con el mensaje de que no existe el curso
            return res.render('cursos', {
                mensaje: 'Hubo un error al eliminar el curso, al parecer no existe en la base de datos.',
                cursos
            })
        }
    } catch (error) {
        console.log(error);
    }
}

// Exportar funciones
export{
    agregarCurso,
    verCurso,
    editarCurso,
    eliminarCurso
}