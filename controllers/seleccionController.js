// Importaciones
import Encuesta from "../models/Encuesta.js";
import Curso from "../models/Curso.js";
import Seleccion from "../models/Seleccion.js";

// Funciones
const agregarSeleccion = async (req, res) =>{

    try {
        let usuario_id = req.body.usuario_id;
        delete req.body.usuario_id;
        
        // Se obtienen las llaves de los datos enviados
        let selecciones = Object.keys(req.body);
    
        // Validacion
        if (selecciones.length > 2 || selecciones.length < 1) {
            // Busca los cursos
            const cursos = await Curso.findAll();
    
            // Si es una seleccion invalida, se renderiza la misma vista y con un mensaje de alerta
            return res.render('home',{
                mensaje: 'Debe seleccionar al menos una asignatura, pero no mas de 2.',
                cursos,
                encuestaStatus: true
            })
        }
    
        // Variables
        let votaciones = [];
        let resultado;
    
        // Buscar las asignaturas
        for (let i = 0; i < selecciones.length; i++) {
            resultado = await Curso.findOne({ where: { nombre: selecciones[i] } })
    
            if (resultado) {
                votaciones.push(resultado.id);
            }
        }
    
        console.log(selecciones);
        console.log(votaciones);
    
        let encuesta = await Encuesta.findOne({ where: { status: true }});
        let cursos = await Curso.findAll();
        
        if (encuesta) {
            
            // Asignacion del id de la encuesta
            let data = { encuesta_id: encuesta.id }

            // Asignando los votos del usuario
            votaciones.forEach( (voto_id, index) => data[`curso_${index + 1}_id`] = voto_id);

            // Asignacion del usuario_id
            data["usuario_id"] = usuario_id || 1

            // Crea la seleccion
            Seleccion.create(data);
            console.log(data);
            
            return res.render('home', {
                cursos,
                usuario_id,
                encuestaStatus: true,
                mensaje:"Seleccion guardada correctamente"
            })
        }else{
            return res.render('home', {
                cursos,
                usuario_id,
                encuestaStatus: true,
                mensaje: "Encuesta no encontrada, algo ocurrio mal."
            })
        }
    
    } catch (error) {
        console.log(error);
    }
}

// Ver selecciones
const verSelecciones = async(req, res) =>{
    
    try {
        // Buscando las selecciones
        const encuestas = await Encuesta.findAll();
        let data = [];

        // for (let i = 0; i < encuesta.length; i++) {
        //     let encuesta = await Encuesta.findOne({ where: { id: selecciones[i]["encuesta_id"]}});

        //     if (encuesta) {
        //         data.push({
        //             id: selecciones[i]["id"],
        //             fecha_inicio: encuesta["fecha_inicio"],
        //             fecha_fin: encuesta["fecha_fin"]
        //         })
        //     }
            
        // }

        // Renderizando la vista
        res.render('selecciones', {
            encuestas
        })
    } catch (error) {
        console.log(error);
    }
}

// Ver una seleccion
const verSeleccion = async(req, res) =>{
    // Id
    const { id } = req.params;

    try {
        let seleccionesDB = await Seleccion.findAll({ where: { encuesta_id: id }});
        seleccionesDB = seleccionesDB.map( seleccion => seleccion['dataValues']);
        let selecciones = [];
        // let curso1, curso2;
        let validacion = [];
        let fecha_inicio, fecha_fin;
        
        if (seleccionesDB.length > 0) {
            for (let i = 0; i < seleccionesDB.length; i++) {
                
                await recopilarSelecciones( seleccionesDB[i]['curso_1_id'], validacion, selecciones);
    
                if (seleccionesDB[i]['curso_2_id']) {
                    await recopilarSelecciones( seleccionesDB[i]['curso_2_id'], validacion, selecciones);
                }
            }
    
            console.log('Las selecciones son');
            console.log(seleccionesDB);
            console.log(selecciones);
    
    
            const {dataValues} = await Encuesta.findOne({ where: {id: seleccionesDB[0]['encuesta_id'] }})
            console.log(dataValues);

            fecha_inicio = dataValues['fecha_inicio'];
            fecha_fin = dataValues['fecha_fin'];
            
            // Ordenar las selecciones de mayor a menor
            selecciones.sort((votoAnterior, votoSiguiente) => votoSiguiente.votos - votoAnterior.votos);
    
            console.log(selecciones);
            // Renderizar la vista
            return res.render('seleccion', {
                fecha_inicio,
                fecha_fin,
                selecciones,
            })
        }


        if (seleccionesDB.length <= 0) {
            return res.render('seleccion', {
                fecha_inicio,
                fecha_fin,
                selecciones: [],
                error: true
            })
        }
    } catch (error) {
        console.log(error);
    }
}

const recopilarSelecciones = async(curso_id, validacion, selecciones)=>{
    // Busqueda del curso
    let curso = await Curso.findOne({ where: { id: curso_id}});

    // Reasignacion para obtener los valores
    curso = curso['dataValues'];


    // Filtrar entre las secciones si ya existe este curso en esta lista
    validacion = selecciones.filter( seleccion => seleccion['id'] === curso['id']);

    // Si no encuentra el curso entre los seleccionados
    if (validacion.length === 0) {
        // Lo agrega a la lista
        selecciones.push({
            id: curso['id'],
            nombre: curso['nombre'],
            votos: 1,
        });
    }else{
        // Busca entre la lista la posicion donde se encuentra el curso
        let cursoPosition = selecciones.findIndex(seleccion => seleccion.id === curso['id']);

        // Actualiza el objeto en esa posicion y le suma un voto
        selecciones[cursoPosition] = {
            id: curso['id'],
            nombre: curso['nombre'],
            votos: selecciones[cursoPosition]['votos'] + 1,
        }
    }
}

export {
    agregarSeleccion,
    verSelecciones,
    verSeleccion
}