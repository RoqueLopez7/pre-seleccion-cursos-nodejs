// Importaciones
import Usuario from "../models/Usuario.js";
import Rol from "../models/Rol.js";
import { Op } from "sequelize";

const obtenerUsuarios = async(req,res) =>{
    try {
        // Buscar usuarios
        let usuarios = await Usuario.findAll();
        usuarios = usuarios.map( usuario => usuario['dataValues']);

        // Declaracion de la variable que contendra el rol
        let rol;

        // Iteramos los usuarios para buscar su rol
        for (let i = 0; i < usuarios.length; i++) {
            // Buscamos su rol
            rol = await Rol.findOne({ where: { id: usuarios[i]['rol_id'] }})
            
            // Y editamos el objeto, solo agregandole el nombre del rol
            usuarios[i] = {
                ...usuarios[i],
                rol_name: rol['dataValues']['nombre']
            }
        }

        // Renderizando la pagina de usuarios
        res.render('usuarios', { usuarios })
    } catch (error) {
        console.log(error);
    }
}


const nuevoUsuario = async (req,res) => {

    let roles = [];
    try {
        // Busqueda de los roles
        roles = await Rol.findAll();

        roles = roles.map( rol => rol['dataValues']);
    } catch (error) {
        console.log(error);
    }

    // Renderizado de la vista de creacion de usuarios
    res.render('nuevo-usuario', { roles });
}

// Funcion para guardar los cambios del usuario
const guardarUsuario = async(req, res) => {
    try {
        // Destructuring
        let {nombre, apellido, correo, clave, rol_id} = req.body;

        // Busqueda de todos los roles
        const roles = await Rol.findAll();

        // Validacion para que todos los campos esten llenos
        if ([nombre,apellido,correo, clave, rol_id].includes('')) {
            // Retorna a la misma vista
            return res.render('nuevo-usuario', {
                mensaje: 'Todos los campos son obligatorios',
                roles
            })
        }

        // si la contraseña tiene menos de 6 caracteres
        if(clave.length < 6){
            // Retorna a la misma vista
            return res.render('nuevo-usuario', {
                mensaje: 'Por seguridad, la contraseña debe tener al menos 6 caracteres',
                roles
            })
        }

        // Formateo del correo
        correo = correo.trim().toLowerCase();

        // Busqueda de otro usuario con el mismo correo
        if(await Usuario.findOne({ where: {correo} })){
            // Retorna a la misma vista
            return res.render('nuevo-usuario', {
                mensaje: 'Ya hay un usuario registrado con ese correo electronico',
                roles
            })
        }

        // Parsear el identificador del rol
        rol_id = parseInt(rol_id);

        // Crea al usuario
        Usuario.create({
            ...req.body,
            correo,
            rol_id
        });

        // Se redirige al usuario a la vista de usuarios
        return res.redirect('/usuarios');
    } catch (error) {
        // Se muestra el error por consola
        console.log(error);
    }
}

const eliminarUsuario = async (req, res) =>{
    try {
        // Tomando el id desde los parametros
        const { id } = req.params;

        // Busqueda de usuarios
        const usuario = await Usuario.findOne({ where: { id }});

        // Si existe el usuario
        if (usuario) {
            // Eliminar al usuario
            usuario.destroy();
        }

        // redirecciona a la tabla de usuarios
        res.redirect('/usuarios');
    } catch (error) {
        console.log(error);
    }
}

const editarUsuario = async (req, res) => {
    try {
        const { id } = req.params;

        const usuario = await Usuario.findOne({ where: { id }});

        const roles = await Rol.findAll();

        res.render('editar-usuario', { usuario, roles})
    } catch (error) {
        console.log(error);
    }
}

const guardarEdicion = async(req, res)=>{
    try {
        // Destructuring
        let {id} = req.params
        let {nombre, apellido, correo, clave, rol_id} = req.body;

        id = parseInt(id);
        // Buscar al usuario
        let usuario = await Usuario.findOne({ where: { id }});

        // Busqueda de todos los roles
        const roles = await Rol.findAll();

        // Validacion para que todos los campos esten llenos
        if ([nombre,apellido,correo, clave, rol_id].includes('')) {
            // Retorna a la misma vista
            return res.render('editar-usuario', {
                mensaje: 'Todos los campos son obligatorios',
                roles,
                usuario: {...req.body, id}
            })
        }

        // si la contraseña tiene menos de 6 caracteres
        if(clave.length < 6){
            // Retorna a la misma vista
            return res.render('editar-usuario', {
                mensaje: 'Por seguridad, la contraseña debe tener al menos 6 caracteres',
                roles,
                usuario: {...req.body, id}
            })
        }

        // Formateo del correo
        correo = correo.trim().toLowerCase();

        console.log(correo);
        console.log(id);
        let busquedaUsuario = await Usuario.findOne({ where: { correo } });

        if (busquedaUsuario) {
            busquedaUsuario = busquedaUsuario['dataValues'];
    
            console.log('Busqueda del usuario');
            console.log(busquedaUsuario);
            // Busqueda de otro usuario con el mismo correo
            if(busquedaUsuario.id !== id){
                // Retorna a la misma vista
                return res.render('editar-usuario', {
                    mensaje: 'Ya hay un usuario registrado con ese correo electronico',
                    roles,
                    usuario: {...req.body, id}
                })
            }
        }

        // Parsear el identificador del rol
        rol_id = parseInt(rol_id);

        console.log('se actualiza');
        console.log({
                ...req.body,
                correo,
                rol_id
            });
        // Actualizar registro
        usuario.update({
            ...req.body,
            correo,
            rol_id
        })

        // redireccion a la vista de usuarios
        res.redirect('/usuarios')
    } catch (error) {
        console.log(error);
    }
}

// Exportar funciones
export{
    obtenerUsuarios,
    nuevoUsuario,
    guardarUsuario,
    eliminarUsuario,
    editarUsuario,
    guardarEdicion
}   