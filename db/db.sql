-- Creacion de la base de datos
CREATE DATABASE pre_seleccion_cursos;

-- uso de la base de datos
USE pre_seleccion_cursos;

-- Creacion de las tablas	
CREATE TABLE `usuarios` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255),
  `apellido` varchar(255),
  `correo` varchar(255),
  `clave` longtext,
  `rol_id` integer,
  `created_at` date,
  `updated_at` date
);

CREATE TABLE `roles` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255),
  `created_at` date,
  `updated_at` date
);

CREATE TABLE `seleccion` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `encuesta_id` integer,
  `curso_1_id` integer,
  `curso_2_id` integer
);

CREATE TABLE `cursos` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255),
  `created_at` date,
  `updated_at` date
);

CREATE TABLE `encuestas` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `fecha_inicio` date,
  `fecha_fin` date,
  `status` boolean,
  `created_at` date,
  `updated_at` date
);

-- Creacion de roles
INSERT INTO pre_seleccion_cursos.roles
(nombre, created_at, updated_at)
VALUES('administrador', NULL, NULL);

INSERT INTO pre_seleccion_cursos.roles
(nombre, created_at, updated_at)
VALUES('estudiante', NULL, NULL);

-- Creacion de usuario admin
INSERT INTO pre_seleccion_cursos.usuarios
(nombre, apellido, correo, clave, rol_id, created_at, updated_at)
VALUES('administrador', 'administrador', 'administrador@gmail.com', 1, NULL, NULL);

-- Asignacion de las relaciones
ALTER TABLE `usuarios` ADD FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`);

ALTER TABLE `usuarios` ADD FOREIGN KEY (`seleccion_id`) REFERENCES `seleccion` (`id`);

ALTER TABLE `encuestas` ADD FOREIGN KEY (`id`) REFERENCES `seleccion` (`encuesta_id`);

ALTER TABLE `cursos` ADD FOREIGN KEY (`id`) REFERENCES `seleccion` (`curso_1_id`);

ALTER TABLE `cursos` ADD FOREIGN KEY (`id`) REFERENCES `seleccion` (`curso_2_id`);