// Importaciones
import express from "express"

// Rutas
import { paginaInicio, autenticarUsuario, paginaHome, paginaCursos, cerrarSesion } from "../controllers/paginasController.js";
import { agregarSeleccion, verSelecciones, verSeleccion } from "../controllers/seleccionController.js";
import { agregarCurso, verCurso, editarCurso, eliminarCurso } from "../controllers/cursoController.js";
import { obtenerEncuestas, agregarEncuesta, verEncuesta, eliminarEncuesta, editarEncuesta } from "../controllers/encuestaController.js";
import { obtenerUsuarios, nuevoUsuario, guardarUsuario, editarUsuario, eliminarUsuario, guardarEdicion } from "../controllers/usuarioController.js";

// Llamamos al enrutador de express
const router = express.Router();

// Endpoint para el inicio
router.get('/', paginaInicio)
router.post('/', autenticarUsuario)

// Endpoint de la zona principal
router.get('/home', paginaHome);
router.post('/home', agregarSeleccion);

// Endpoint para los cursos
router.get('/cursos', paginaCursos);
router.get('/curso/:id', verCurso);
router.post('/cursos', agregarCurso);
router.post('/editar-curso/:id', editarCurso);
router.get('/eliminar-curso/:id', eliminarCurso);

// Endpoints para las encuestas
router.get('/encuestas', obtenerEncuestas);
router.get('/encuesta/:id', verEncuesta);
router.get('/eliminar-encuesta/:id', eliminarEncuesta);
router.post('/editar-encuesta/:id', editarEncuesta);
router.post('/encuestas', agregarEncuesta);

// Endpoints para las selecciones
router.get('/selecciones', verSelecciones);
router.get('/seleccion/:id', verSeleccion);

// Endpoint para usuarios
router.get('/usuarios', obtenerUsuarios);
router.get('/nuevo-usuario', nuevoUsuario);
router.get('/editar-usuario/:id', editarUsuario);
router.get('/eliminar-usuario/:id', eliminarUsuario);
router.post('/nuevo-usuario', guardarUsuario);
router.post('/editar-usuario/:id', guardarEdicion);

// Endpoint para cerrar sesion
router.get('/cerrar-sesion', cerrarSesion);

// Exportamos el enrutador
export default router;